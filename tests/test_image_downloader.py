import os
import sys
import pytest
import requests
import subprocess

from image_downloader import (
    download_image, get_name_from_response, is_image_url
)


class RequestTestObject:
    def __init__(self, filename):
        content_disposition = f'attachment; filename={filename}'
        self.headers = {
            'Content-Disposition': content_disposition,
            'content-disposition': content_disposition
        }


class TestFunctions:
    TEST_IMAGE = (
        'https://cdn.pixabay.com/photo/2016/09/01/10/23/'
        'image-1635747_960_720.jpg'
    )

    def test_is_image_url_true(self):
        r = requests.get(self.TEST_IMAGE, stream=True)
        assert is_image_url(r) is True

    def test_is_image_url_false(self):
        r = requests.get('https://google.com/', stream=True)
        assert is_image_url(r) is False

    def test_get_name_from_response_url(self):
        r = requests.get(f'{self.TEST_IMAGE}?someParam=true', stream=True)
        assert get_name_from_response(r) == 'image-1635747_960_720.jpg'

    def test_get_name_from_response_url_with_parameters(self):
        r = requests.get(self.TEST_IMAGE, stream=True)
        assert get_name_from_response(r) == 'image-1635747_960_720.jpg'

    def test_get_name_from_response_content_disposition(self):
        r = RequestTestObject('image name.jpeg')
        assert get_name_from_response(r) == 'image name.jpeg'

    def test_get_name_from_response_content_disposition_quotes(self):
        r = RequestTestObject("'image name.jpeg'")
        assert get_name_from_response(r) == 'image name.jpeg'

    def test_get_name_from_response_content_disposition_doble_quotes(self):
        r = RequestTestObject('"image name.jpeg"')
        assert get_name_from_response(r) == 'image name.jpeg'

    def test_download_image(self):
        r = requests.get(self.TEST_IMAGE, stream=True)
        size = int(r.headers['Content-Length'])
        image_name = 'image name.jpg'
        directory = ''
        download_image(r, image_name, directory)
        filepath = os.path.join(directory, image_name)
        assert os.path.exists(filepath)
        assert os.path.getsize(filepath) == int(r.headers['Content-Length'])
        os.remove(filepath)


class TestScript:
    OUTPUT_TEXTS = {
        'CONNECTION_ERROR': b"ERROR: Couldn't connect with https://asnjas.",
        'SCHEMA_ERROR': (
            b"ERROR: Invalid URL. Missing schema. "
            b"Perhaps you meant http://asnjas"
        ),
        '403_ERROR': (
            b"ERROR: FORBIDDEN. Request returned 403 FORBIDDEN status code. "
            b"https://httpstat.us/403"
        ),
        '404_ERROR': (
            b"ERROR: NOT FOUND. Request returned 404 NOT FOUND status code. "
            b"https://httpstat.us/404"
        ),
        'STATUS_CODE_ERROR': (
            b"ERROR: Request returned a 500 error status code. "
            b"https://httpstat.us/500"
        ),
        'CONTENT_TYPE': b"Content-Type header must contain 'image'."
    }
    IMAGE_NAMES = ['image-1635747_960_720.jpg', 'my own image.jpg']
    folder = '..' if os.getcwd().endswith('/tests') else '.'
    folder = os.path.join(folder, '')

    def test_script_without_parameter(self):
        p = self._run_downloader_script([f'{self.folder}image_downloader.py'])
        assert b'the following arguments are required: filename' in p.stderr

    def test_script_with_non_existing_file(self):
        p = self._run_downloader_script(
            [f'{self.folder}image_downloader.py', 'non_existing_file.txt']
        )
        assert b'ERROR: IO Error' in p.stdout

    def test_script_with_example_file(self):
        p = self._run_downloader_script([
            f'{self.folder}image_downloader.py',
            f'{self.folder}test_images.txt',
            '-v'
        ])

        for key in self.OUTPUT_TEXTS:
            assert self.OUTPUT_TEXTS[key] in p.stdout

        for name in self.IMAGE_NAMES:
            image_path = os.path.join(f'images', name)
            assert os.path.exists(image_path)
            os.remove(image_path)

    def test_script_non_existing_directory(self):
        p = self._run_downloader_script([
            f'{self.folder}image_downloader.py',
            f'{self.folder}test_images.txt',
            f'{self.folder}xxxMYIMAGESDIRxxx',
            '-v'
        ])
        assert os.path.exists(f'{self.folder}xxxMYIMAGESDIRxxx')
        assert b"Directory doesn't exist. Creating..." in p.stdout

        for name in self.IMAGE_NAMES:
            os.remove(f'{self.folder}xxxMYIMAGESDIRxxx/{name}')
        os.rmdir(f'{self.folder}xxxMYIMAGESDIRxxx')

    def test_script_non_existing_directory_without_permissions(self):
        p = self._run_downloader_script([
            f'{self.folder}image_downloader.py',
            f'{self.folder}test_images.txt',
            f'/xxxMYIMAGESDIRxxx'
        ])
        assert b"ERROR: Can't create directory. Check permissions." in p.stdout

    def test_script_without_permissions(self):
        p = self._run_downloader_script([
            f'{self.folder}image_downloader.py',
            f'{self.folder}test_images.txt',
            '/'
        ])
        assert b'ERROR: Not enough permissions for path' in p.stdout

    def test_script_with_empty_file(self):
        f = open(f'{self.folder}test_empty_file.txt', 'x')
        f.close()
        p = self._run_downloader_script([
            f'{self.folder}image_downloader.py',
            f'{self.folder}test_empty_file.txt',
        ])
        os.remove(f'{self.folder}test_empty_file.txt')
        assert p.stdout == b''
        assert p.stderr == b''

    def _run_downloader_script(self, command):
        return subprocess.run(command, capture_output=True)
