# Python Image Downloader

This is a simple CLI image downloader. It takes a plaintext file with one url
per line and downloads each of them if they're images.

You can optionally supply a destination directory by specifying a second
argument.

It shows a progress bar for each image and also name and size if verbose mode
is active (-v flag).

## Installation

#### Requirements

* **Python >= 3.6**

### Instructions

* Clone repository: 
`git clone https://gitlab.com/francopelusso/python-image-downloader/`
* Create a virtualenv: `python3 -m venv virtualenvname`
* Activate virtualenv: `source virtualenvname/bin/activate` or 
`virtualenvname\Scripts\activate` for Windows.
* Move into repo's directory: `cd python-image-downloader`
* Install project requirements: `pip install -r requirements.txt`
* If you want to contribute, install requirements\_dev: 
`pip install -r requirements_dev.txt`
* You're done. Now just run the script!

## Usage

Just run script through terminal: `./image_downloader.py filename.txt` or
`python image_downloader.py filename.txt`

### Arguments
* **filename**: *str* - Name of file containing images' urls. Relative or 
absolute `/home/username/python-image-downloader/filename.txt` or 
`filename.txt` if in current directory.
* **directory**: *str* (optional) - Name of destination directory. If it 
doesn't exist, it will be created. If not enough permissions, an alert will be 
raised.
* **--verbose | -v**: *flag* (optional) - Just add *--verbose* or *-v* for 
extra output messages.

## Tests

**These project uses [pytest](https://docs.pytest.org/en/latest/) to run** 
**automated tests.**

Just move into project directory and run `pytest` in your command line.

## GitLab
These project will check if code style meets 
[**PEP8**](https://www.python.org/dev/peps/pep-0008/) standards and run 
tests automatically when changes are pushed to gitlab.

This behaviour is configured by **.gitlab-ci.yml** file and makes use of the 
aforementioned **pytest** package together with the **pycodestyle** package.