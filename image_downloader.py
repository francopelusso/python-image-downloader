#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import requests
import re
from clint.textui import progress
from math import ceil

SCRIPT_DESCRIPTION = (
    'Download image files listed in text file. '
    'Must have only one url per line. '
    'Custom filename can be supplied on the same line after a whitespace. '
    'Example: https://myweb.com/image.jpeg my awesome image.jpeg'
)

ERRORS = {
    'NO_SCHEMA': 'Invalid URL. Missing schema. Perhaps you meant http://{}',
    'CONNECTION': "Couldn't connect with {}. Check if url is correct.",
    'NOT_IMAGE': "Invalid URL. Isn't an image: {}",
    '404': 'NOT FOUND. Request returned 404 NOT FOUND status code. {}',
    '403': 'FORBIDDEN. Request returned 403 FORBIDDEN status code. {}',
    'CODE': 'Request returned a {} error status code. {}',
    'PERMISSION': 'Not enough permissions for path {}',
    'IO': 'IO Error. Check available disk space and existence of path {}'
}

CHUNK_SIZE = 1024


def is_image_url(response):
    """Check for image in response's content-type. Return Boolean."""
    return 'image' in response.headers['Content-Type']


def download_image(response, image_name, directory, verbose=False):
    """Stream through response and save it in local disk."""
    fullpath = os.path.join(directory, image_name)
    try:
        with open(fullpath, 'wb') as writer:
            response_size = int(response.headers['Content-Length'])
            if verbose:
                print(f'Image size is {response_size/1000} kB.')
            chunk_size = CHUNK_SIZE
            expected_size = ceil(response_size / chunk_size)

            # Use clint package to show a progress bar on terminal.
            content_iterator = progress.bar(
                response.iter_content(chunk_size=chunk_size),
                label=f'Downloading {image_name} ',
                expected_size=expected_size
            )

            for chunk in content_iterator:
                writer.write(chunk)
    except PermissionError:
        print(f"ERROR: {ERRORS['PERMISSION'].format(fullpath)}")
    except IOError:
        print(f"ERROR: {ERRORS['IO'].format(fullpath)}")


def get_name_from_response(response):
    """Return filename string from content-disposition or url."""
    content_disposition = response.headers.get('content-disposition', '')
    filename = re.search('filename=(.+)', content_disposition)

    if filename:
        # Remove quotations from filename.
        filename = filename.group(1).strip('"').strip("'")
    else:
        filename = response.url.split('/')[-1]
        # Remove parameters from URL.
        filename = filename.split('?')[0]

    return filename


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=SCRIPT_DESCRIPTION)
    parser.add_argument('filename', help='Text file path.')
    parser.add_argument(
        'directory',
        nargs='?',
        default='images',
        help='Download destination.'
    )
    parser.add_argument(
        '-v',
        '--verbose',
        action='store_true',
        help='Output more text.'
    )
    args = parser.parse_args()

    if not os.path.isdir(args.directory):
        if args.verbose:
            print("Directory doesn't exist. Creating...")
        try:
            os.mkdir(args.directory)
        except PermissionError:
            print("ERROR: Can't create directory. Check permissions.")

    try:
        with open(args.filename) as data_file:
            for line in data_file:
                # Remove whitespaces and newline characters.
                image_data = line.strip().split(' ')
                url = image_data[0]

                # Stream: Get headers. Don't get content until required.
                try:
                    r = requests.get(url, stream=True)
                except requests.exceptions.MissingSchema:
                    print(f"ERROR: {ERRORS['NO_SCHEMA'].format(url)}")
                    continue
                except requests.exceptions.ConnectionError:
                    print(f"ERROR: {ERRORS['CONNECTION'].format(url)}")
                    continue
                if r.status_code >= 400:
                    if r.status_code == 404:
                        print(f"ERROR: {ERRORS['404'].format(url)}")
                    elif r.status_code == 403:
                        print(f"ERROR: {ERRORS['403'].format(url)}")
                    else:
                        error = ERRORS['CODE'].format(r.status_code, url)
                        print(f"ERROR: {error}")
                    continue

                if not is_image_url(r):
                    print(f"ERROR: {ERRORS['NOT_IMAGE'].format(url)}")
                    if args.verbose:
                        print("Content-Type header must contain 'image'.")
                    continue

                if len(image_data) == 1:
                    image_name = get_name_from_response(r)
                else:
                    image_name = " ".join(image_data[1:])
                if args.verbose:
                    print(f'Image name is {image_name}.')

                download_image(r, image_name, args.directory, args.verbose)
    except PermissionError:
        print(f"ERROR: {ERRORS['PERMISSION'].format(args.filename)}")
    except IOError:
        print(f"ERROR: {ERRORS['IO'].format(args.filename)}")
